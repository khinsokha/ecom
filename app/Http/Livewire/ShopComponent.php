<?php

namespace App\Http\Livewire;

use App\Models\Product;
use Livewire\Component;
use Livewire\WithPagination;
use Cart;

class ShopComponent extends Component
{    
    use WithPagination;

    public function store($product_id,$product_name,$prodct_price)
    {
        Cart::add($product_id,$product_name,1,$product_name,$prodct_price)->associate('App\Model\Product');
        session()->flash('success_message','Item add in cart');
        return redirect('product.cart');
    }
    public function render()
    {   
        $products = Product::paginate(6);
        return view('livewire.shop-component',['products'=> $products])->layout('layouts.base');
        // return view('livewire.shop-component')->layout('layouts.base');
    }
}
